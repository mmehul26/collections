//
//  ViewController.m
//  CollectionsInObjective-C
//
//  Created by Mehul Makwana on 01/09/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//Array Literals
    //NSArray
    //A static ordered collection of objects.
    
    //NSMutableArray
    //A dynamic ordered collection of objects.
    
    NSArray *array = @[@"Hello, World!", @42];
    NSMutableArray *raa1 = [@[@"Mehul",@"Makwana"]mutableCopy];
    
    NSLog(@"%lu",(unsigned long)array.count);
    
    id str1 = array[0];  //Subscript
    id str2 = [array objectAtIndex:1];
    
    NSLog(@"%@ %@",str1,str2);

/*
 Methods to Override
 NSMutableArray defines five primitive methods:
 insertObject:atIndex:
 removeObjectAtIndex:
 addObject:
 removeLastObject
 replaceObjectAtIndex:withObject:
 */
    
    
//Dictionary Literals
    //NSDictionary
    //A static collection of objects associated with unique keys.
    
    //NSMutableDictionary
    //A dynamic collection of objects associated with unique keys.
    
    NSDictionary *dictionary = @{  @"helloString" : @"Hello, World!",
                                    @"magicNumber" : @42,
                                    @"aValue" : @true
                                };

    NSMutableDictionary *mutableDictionary = [@{  @"helloString" : @"Hello, World!",
                                                  @"magicNumber" : @42,
                                                  @"aValue" : @true
                                                  }mutableCopy];
    
    
    // In a subclass NSMutableDictionary, you must override both of its primitive methods:
    // setObject:forKey:
    // removeObjectForKey:
    
//Set Literals
    
    //NSSet
    // A static unordered collection of unique objects.
    
    //NSCountedSet
    // A mutable, unordered collection of distinct objects that may appear more than once in the collection.
    
    //NSMutableSet
    // A dynamic unordered collection of unique objects.
    
    

    NSSet *set = [NSSet setWithArray:@[@"Eezy",@"Eezy",@"Tutorials",@"Website"]];
    NSLog(@"%@ %ld", set,set.count);
    NSCountedSet *countedSet = [[NSCountedSet alloc]initWithSet:set];
    NSLog(@"%@ %ld",countedSet,countedSet.count);
    
    
    NSMutableSet *set1 = [NSMutableSet set];
    [set1 addObject:@"Eezy"];
    NSLog(@"%@ %ld",set1,set1.count);

}



@end
